OpenRA Master Server
====================

Scripts that make up the [OpenRA](http://open-ra.org) project's master server for game broadcasts.

Supported output formats
------------------------
 * MiniYAML: http://master.open-ra.org/list.php
 * JSON: http://master.open-ra.org/list_json.php

Stats
-----
 * Uptime: http://stats.pingdom.com/zaegmvlagqpe/788265
 * Players: http://openra.ipdx.ru/graphs.html
 * Origins: http://res0l.net/src/LiveORA/map.html
